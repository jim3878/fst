﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToTitle : MonoBehaviour
{

    public void Back()
    {

        DOTween.CompleteAll();

        ShowOver.sysc.allowSceneActivation = true;
    }
}
