﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StopBgm : MonoBehaviour
{
    public AudioSource bgm;
    void Pause(float fade)
    {
        bgm.DOFade(0, fade);
    }
}
