﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kawabanga : MonoBehaviour
{
    public KawabangaCap cap;
    //public GameObject ground;
    public GameObject ground;
    public float capHigh = 2.28f;

    void Update()
    {

    }
    private void OnTriggerStay2D(Collider2D other)
    {
        var c = other.gameObject.GetComponentInParent<KawabangaCap>();
        if (c == null || c.GetComponent<Pickable>().isPicked)
            return;
        cap = c;
        ground.layer = 10;
        cap.kawabanga = this;
        cap.transform.position = new Vector3(transform.position.x, transform.position.y + capHigh, cap.transform.position.z);
        //Debug.Log("close " + other);
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        var c = other.gameObject.GetComponentInParent<KawabangaCap>();
        if (c == null || c != cap)
            return;
        cap.kawabanga = null;
        cap = null;
        ground.layer = 9;
        Debug.Log("open " + other);
    }
}
