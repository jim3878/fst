﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KawabangaCap : MonoBehaviour
{
    public Pickable pickable;
    public Kawabanga kawabanga;
    public GameObject cover, stand;

    private void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        if (!pickable.isPicked && kawabanga != null)
        {
            stand.SetActive(false);
            cover.SetActive(true);
        }
        else
        {
            cover.SetActive(false);
            stand.SetActive(true);
        }
    }

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    var k = collision.GetComponent<Kawabanga>();
    //    if (k != null)
    //        kawabanga = k;
    //}
}
