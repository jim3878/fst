﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainUser : MonoBehaviour
{
    public Pickable pickable;
    public Chain chain;


    // Update is called once per frame
    void Update()
    {
        if (pickable.isPicked)
        {
            chain.snapOffB();
            Destroy(pickable.gameObject);
        }
    }
}
