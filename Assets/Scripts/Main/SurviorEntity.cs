﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum SurviorAniState
{
    run,
    idle,
    fall,
    die,
}

public class SurviorEntity : MonoBehaviour
{
    public SurviorAniState state;
    public SurviorAniState _preState;
    public SpriteSheet runSheet;
    public SpriteSheet fallSheet;
    public SpriteSheet dieSheet;
    public SpriteSheet idleSheet;
    public GridAnimator animator;
    public SpriteRenderer sr;
    public Rigidbody2D rigid;
    public bool isOnGround;
    public bool isDie;
    public bool isIdle;
    public bool hasElectric;
    public Color electricColor;
    public bool isMyPet = false;
    public float speed;
    public float speedBuffer;
    public GameObject electric;
    public Vector3 runDirector = Vector3.right;
    public Action onExit;
    public Action enterDie, enterFall, enterIdle, enterRun;
    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        if (isMyPet)
        {
            ShowOver.isGG = false;
            ShowOver.se = this;
        }
        TickState();
        animator.Tick();
        Run();
        TickElectric();
    }

    public void TickElectric()
    {
        electric.SetActive(hasElectric);
        if (hasElectric)
            sr.color = Color.Lerp(sr.color, electricColor, 0.2f);
    }

    public void Init()
    {
        enterDie += EnterDieAnima;
        enterFall += EnterFallAnmia;
        enterIdle += EnterIdleAnmia;
        enterRun += EnterRunAnima;
        enterFall?.Invoke();
    }

    void Run()
    {
        if (state != SurviorAniState.run)
            return;
        rigid.MovePosition(rigid.transform.position +
            runDirector.normalized * speed * speedBuffer * Time.deltaTime);
    }

    void EnterRunAnima()
    {
        animator.SetCurrSheets(runSheet);
    }

    void EnterDieAnima()
    {
        animator.SetCurrSheets(dieSheet);
    }

    void EnterIdleAnmia()
    {
        animator.SetCurrSheets(idleSheet);
    }

    void EnterFallAnmia()
    {
        animator.SetCurrSheets(fallSheet);
    }

    void TickState()
    {
        if (isDie)
        {
            if (_preState != SurviorAniState.die)
            {
                ExitState();
                enterDie?.Invoke();
            }
            state = SurviorAniState.die;
        }
        else if (!isOnGround)
        {
            if (_preState != SurviorAniState.fall)
            {
                ExitState();
                enterFall?.Invoke();
            }
            state = SurviorAniState.fall;
        }
        else if (isIdle)
        {
            if (_preState != SurviorAniState.idle)
            {
                ExitState();
                enterIdle?.Invoke();
            }
            state = SurviorAniState.idle;
        }
        else
        {
            if (_preState != SurviorAniState.run)
            {
                ExitState();
                enterRun?.Invoke();
            }
            state = SurviorAniState.run;
        }
        _preState = state;
    }

    private void ExitState()
    {
        onExit?.Invoke();
        onExit = null;

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
            isOnGround = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.gameObject.layer == 10)
            isOnGround = false;
    }
}
