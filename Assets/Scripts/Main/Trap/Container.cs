﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Container : MonoBehaviour
{
    public bool isBlowed = false;
    public SpriteRenderer sr;
    public Sprite breakContainer;
    public Rigidbody2D rigid;
    public Vector2 director;
    public float power;
    public Vector2 position;
    public Vector3 origPos;
    public StandArear area;

    private void Start()
    {
        origPos = transform.position;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log(collision);
        if (isBlowed || collision.gameObject.layer != 11)
            return;
        isBlowed = true;
        gameObject.layer = 9;
        Boom();
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + new Vector3(position.x, position.y), 1f);
        Gizmos.DrawLine(transform.position + new Vector3(position.x, position.y),
            transform.position + new Vector3(position.x, position.y) + new Vector3(director.x, director.y).normalized * power);
    }

    [ContextMenu("boom")]
    public void Boom()
    {
        rigid.mass = 0.02f;
        rigid.AddForceAtPosition(position + director.normalized * power,
            transform.position + new Vector3(position.x, position.y));
        
        sr.sprite = breakContainer;
        StartCoroutine(Destory());
        area.ReleasAll();
    }
    IEnumerator Destory()
    {
        yield return new WaitForSeconds(2f);
        GameObject.Destroy(gameObject);

    }

    [ContextMenu("RestContainer")]
    public void ResetContainer()
    {
        transform.position = origPos;
        transform.rotation = Quaternion.identity;
    }
}
