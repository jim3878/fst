﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricShockZone : MonoBehaviour
{
    public bool isTurnOn;
    public GameObject[] electrics;
  

    private void Update()
    {
        foreach(var e in electrics)
        {
            e.SetActive(isTurnOn);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var se = collision.GetComponent<SurviorEntity>();
        if (isTurnOn && se != null)
        {
            se.isDie = true;
            se.hasElectric = true;
            se.rigid.AddForce(new Vector2(-1, 1f)*120);
        }
    }
}
