﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOn : MonoBehaviour
{
    public SpriteRenderer outside;
    public Color floorCol, outsideCol;
    public bool IsGodEnter;
    public bool IsGirlEnter;
    public float turnSpeed = 0.3f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        TickColor();
    }

    void TickColor()
    {
        if (IsGodEnter || IsGirlEnter)
            outside.color = Color.Lerp(outside.color, floorCol, turnSpeed);
        else
            outside.color = Color.Lerp(outside.color, outsideCol, turnSpeed);
    }
}
