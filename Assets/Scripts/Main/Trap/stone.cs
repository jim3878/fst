﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class stone : MonoBehaviour
{
    public GameObject lastBridge;
    public Pickable pickable;
    public bool isPicked = false;
    public WoodBox ground;
    public Transform wood;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!isPicked && pickable.isPicked)
        {
            isPicked = true;
            wood.DORotate(Vector3.zero, 1f).SetEase(Ease.OutBounce)
                .OnComplete(() =>
                {
                    ground.isBridged = true;
                    wood.gameObject.SetActive(false);
                    lastBridge.gameObject.SetActive(true);
                });
        }

    }
}
