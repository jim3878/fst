﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenTriangle : MonoBehaviour
{
    public WoodBridge bridge;
    //public GameObject ground;
    public GameObject ground;
    public float bridgeAngle;
    public Vector3 bridgePos;
    public Collider2D triangleCollider;
    public float capHigh = 2.28f;

    void Update()
    {

    }
    private void OnTriggerStay2D(Collider2D other)
    {
        var b = other.gameObject.GetComponentInParent<WoodBridge>();
        if (b == null || b.GetComponent<Pickable>().isPicked)
            return;
        bridge = b;
        ground.layer = 10;
        //bridge.kawabanga = this;
        bridge.transform.position = new Vector3(transform.position.x + bridgePos.x, 
            transform.position.y + bridgePos.y,
            bridge.transform.position.z);
        bridge.transform.rotation = Quaternion.Euler(0, 0, bridgeAngle);
        triangleCollider.isTrigger = false;
        //Debug.Log("close " + other);
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        var b = other.gameObject.GetComponentInParent<WoodBridge>();
        if (b == null || b != bridge)
            return;
        bridge.transform.rotation = Quaternion.Euler(0, 0, 0);
        bridge = null;
        ground.layer = 9;
        triangleCollider.isTrigger = true;

        //Debug.Log("open " + other);
    }
}
