﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlInBuilding : MonoBehaviour
{
    public TurnOn turnOn;


    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.GetComponent<SurviorEntity>())
            turnOn.IsGirlEnter = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<SurviorEntity>())
            turnOn.IsGirlEnter = false;
    }
}
