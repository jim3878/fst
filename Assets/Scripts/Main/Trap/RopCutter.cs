﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopCutter : MonoBehaviour
{
    public ElectricShockZone box, rope;
    public Pickable pickable;
    public Chain chain;


    // Update is called once per frame
    void Update()
    {
        if (pickable.isPicked)
        {
            box.isTurnOn = false;
            rope.isTurnOn = true;
            chain.snapOffA();
            Destroy(pickable.gameObject);
        }
    }
}
