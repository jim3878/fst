﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodBridge : MonoBehaviour
{
    public WoodBox woodBox;
    public Pickable pickable;
    public Rigidbody2D rigid;
    
    private void Update()
    {
        woodBox.isBridged = !pickable.isPicked;
        if (pickable.isPicked)
            transform.rotation = Quaternion.identity;
       //rigid.constraints = RigidbodyConstraints2D.FreezeRotation 
    }
}
