﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricBox : MonoBehaviour
{
    public SpriteRenderer sr;
    public Color on, off;
    public ElectricShockZone trigger;
    public float colorSpeed;

    // Update is called once per frame
    void Update()
    {
        if (trigger.isTurnOn)
            sr.color = Color.Lerp(sr.color, on, colorSpeed);
        else
            sr.color = Color.Lerp(sr.color, off, colorSpeed);
    }
}
