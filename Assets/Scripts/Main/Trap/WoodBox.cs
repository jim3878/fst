﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodBox : MonoBehaviour
{
    public bool isBridged = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isBridged)
            gameObject.layer = 10;
        else
            gameObject.layer = 9;
    }
}
