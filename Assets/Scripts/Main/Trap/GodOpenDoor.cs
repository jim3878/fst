﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodOpenDoor : MonoBehaviour
{
    public TurnOn turnOn;
    public StandArear arear;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerEntity>())
        {
            turnOn.IsGirlEnter = true;
            arear.ReleasAll();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerEntity>())
        {
            turnOn.IsGirlEnter = false;
            arear.isReleased = false;
        }
    }
}
