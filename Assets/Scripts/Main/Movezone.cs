﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movezone : MonoBehaviour
{
    public float speed;
    public AnimationCurve curve;
    public float duration;
    public float startTime;
    public bool canMove=false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!canMove)
            return;
        transform.position += Vector3.right * Time.deltaTime * speed * curve.Evaluate(( Time.time - startTime) / duration);
    }
}
