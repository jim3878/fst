﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedTune : MonoBehaviour
{
    public float buffer = 1.2f;
    public Vector3 runDir = Vector3.right;
    private void OnTriggerStay2D(Collider2D collision)
    {
        var e = collision.GetComponent<SurviorEntity>();
        if (e != null)
        {
            e.speedBuffer = buffer;
            e.runDirector = runDir;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        var e = collision.GetComponent<SurviorEntity>();
        if (e != null)
        {
            Debug.Log("exit "+e.gameObject);
            e.speedBuffer = 1;
            e.runDirector = Vector3.right;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + this.runDir);
    }
}
