﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ShowOver : MonoBehaviour
{
    public Image bg;
    public Image text;
    public Color bgColor;
    public Color txtColor;
    public float textSize;
    public float duration;
    public Button continued;
    public static SurviorEntity se;
    public static bool isGG = false;
    public AudioSource audioSource;
    public AudioClip clip;
    public static AsyncOperation sysc;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(bg);
         sysc = SceneManager.LoadSceneAsync(0);
        sysc.allowSceneActivation = false;
    }

    public void GG()
    {
        bg.DOColor(bgColor, duration);
        text.DOColor(txtColor, duration);

        audioSource.DOFade(0, 0.5f);
        text.transform.DOScale(textSize, duration).OnComplete(
            () =>
            {
                Cursor.visible = true;
                continued.gameObject.SetActive(true);
                continued.onClick.AddListener(() =>
                {
                    Debug.Log("continu");
                    DOTween.CompleteAll();
                    
                    sysc.allowSceneActivation = true;
                });
            });
    }

    // Update is called once per frame
    void Update()
    {
        if (se != null && se.isDie && !isGG)
        {
            isGG = true;
            GG();
        }
    }
}
