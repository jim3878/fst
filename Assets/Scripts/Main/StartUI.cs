﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class StartUI : MonoBehaviour
{
    public AudioClip bgm;
    public AudioSource bgmPlayer;
    public GameObject startPage;
    public SurviorEntity[] ses;
    public Image head, e1, e2, title, bg;
    public Button startBtn;
    public Movezone mz;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var s in ses)
            s.enabled = false;
        bgmPlayer.clip = bgm;
        bgmPlayer.volume = 0.8f;
        bgmPlayer.Play();
        bgmPlayer.loop = true;
        Cursor.visible = true;
        title.DOColor(new Color(1, 1, 1, 0.5f), 1f).SetLoops(-1, LoopType.Yoyo);
        head.DOColor(new Color(1, 1, 1, 0.8f), 2f);
        e1.DOColor(new Color(1, 1, 0.5f, 0.5f), 1f).SetDelay(2f).OnComplete(() =>
        {
            e1.DOColor(new Color(1, 1, 0.5f, 0.2f), 1f).SetLoops(-1, LoopType.Yoyo);
        });
        e2.DOColor(new Color(1, 1, 0.5f, 0.5f), 1f).SetDelay(2f).OnComplete(() =>
        {
            e2.DOColor(new Color(1, 1, 0.5f, 0.2f), 1f).SetLoops(-1, LoopType.Yoyo);
            startBtn.gameObject.SetActive(true);
            startBtn.onClick.AddListener(() =>
            {
                mz.canMove = true;
                mz.startTime = Time.time;
                startPage.gameObject.SetActive(false);
                var col = bg.color;
                bg.DOColor(new Color(col.r, col.g, col.b, 0f), 0.5f);
                Cursor.visible = false;
                foreach (var s in ses)
                    s.enabled = true;

            });
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
}
