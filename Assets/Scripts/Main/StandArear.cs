﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandArear : MonoBehaviour
{
    public List<SurviorEntity> surviorEntities = new List<SurviorEntity>();
    public bool isReleased = false;
    public float maxDelay = 1f;
    public float mixDelay = 0.2f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isReleased && collision.gameObject.layer == 8)
        {
            var entity = collision.GetComponent<SurviorEntity>();
            StartCoroutine(Stand(entity, entity.isMyPet ? maxDelay : Random.Range(mixDelay, maxDelay)));
            this.surviorEntities.Add(entity);
        }
    }

    public void ReleasAll()
    {
        isReleased = true;
        foreach (var e in surviorEntities)
        {
            e.isIdle = false;
        }
        surviorEntities.Clear();
    }

    IEnumerator Stand(SurviorEntity entity, float time)
    {
        yield return new WaitForSeconds(time);
        if (surviorEntities.Contains(entity))
            entity.isIdle = true;
    }
}
