﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Orz.DoTalk
{
    public abstract class StateLeafNode : LeafNode
    {
        public sealed override TickResult Tick(NodeWorkData wData)
        {
            var context = GetContext(wData);
            if (context.state == State.enter)
            {
                Enter(wData);
                context.state = State.excute;
            }

            if (context.state == State.excute)
            {
                var result = Excute(wData);
                if (result == TickResult.done)
                    context.state = State.exit;
                else
                    return TickResult.running;
            }

            //context.state == State.exit
            Exit(wData);
            context.state = State.enter;
            return TickResult.done;

        }

        /// <summary>
        /// 因為父子類別的節點會共用一個context
        /// 於是有兩個問題必需解決:
        /// 1.子類別的context須繼承至父類別。
        /// 2.context的型別須由子類別決定。
        /// 
        /// 此方法的目的為:
        /// 1.父類別的context完全由此方法取得故可確保context的型別由子類別決定
        /// 2.子類別取得子context後轉型成父context型別在回傳給父類別，確保兩context的繼承關係
        /// 
        /// </summary>
        /// <param name="wData"></param>
        /// <returns></returns>
        protected abstract StateLeafContext GetContext(NodeWorkData wData);

        protected virtual void Enter(NodeWorkData wData) { }
        protected virtual TickResult Excute(NodeWorkData wData) { return TickResult.done; }
        protected virtual void Exit(NodeWorkData workData) { }

        public enum State
        {
            enter,
            excute,
            exit,
        }

        public class StateLeafContext : NodeContext
        {
            public State state = State.enter;
        }
    }


}