﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Orz.DoTalk
{
    public enum TickResult
    {
        running,
        //idle,
        done
    }
    public abstract class BaseNode
    {
        
        private int _maxChild;
        protected int maxChild { get { return _maxChild; } }
        protected List<BaseNode> childList;
        public BaseNode(int maxChild)
        {
            _maxChild = maxChild;
            childList = new List<BaseNode>();
        }

        public void AddChildNode(BaseNode child)
        {
            if (_maxChild < 0)
                childList.Add(child);
            else if (childList.Count < _maxChild)
                childList.Add(child);
        }

        protected BaseNode GetChildNode(int idx)
        {
            return childList[idx];
        }

        public int GetChildNodeCount()
        {
            return childList.Count;
        }

        protected T GetContext<T>(NodeWorkData workData) where T : NodeContext, new()
        {
            int uniqueKey = GetUniqueKey();
            if (!workData.context.ContainsKey(uniqueKey))
            {
                workData.context.Add(uniqueKey, new T());
            }
            return workData.context[uniqueKey] as T;
        }

        public abstract TickResult Tick(NodeWorkData wData);

        public abstract bool Evaluate(NodeWorkData wData);

        public virtual int GetUniqueKey()
        {
            return GetHashCode();
        }
    }

}