﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Orz.DoTalk
{
    public class NodeWorkData
    {
        public Dictionary<int, NodeContext> context = new Dictionary<int, NodeContext>();

        public NodeWorkData()
        {
        }
    }

    public class NodeContext {
    }
}