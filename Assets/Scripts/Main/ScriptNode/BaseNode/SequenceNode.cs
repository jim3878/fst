﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Orz.DoTalk
{
    public class SequenceNode : BaseNode
    {
        public SequenceNode() : base(-1) { }

        public override bool Evaluate(NodeWorkData wData)
        {
            var context = GetContext<SequenceNodeContext>(wData);
            var currNodeIndex = context.currentNodeIndex;
            if (!IsCurrentIndexValid(currNodeIndex))
                currNodeIndex = 0;

            //尋找第一個可執行的子節點
            while (!childList[currNodeIndex].Evaluate(wData))
            {
                currNodeIndex++;
                if (!IsCurrentIndexValid(currNodeIndex))
                {
                    //沒有可執行子節點
                    GetContext<SequenceNodeContext>(wData).currentNodeIndex = currNodeIndex;
                    return false;
                }
            }
            //發現可執行子節點
            GetContext<SequenceNodeContext>(wData).currentNodeIndex = currNodeIndex;
            return true;
        }

        private bool IsCurrentIndexValid(int idx)
        {
            return idx >= 0 && idx < childList.Count;
        }

        public override TickResult Tick(NodeWorkData wData)
        {
            var context = GetContext<SequenceNodeContext>(wData);
            if (!IsCurrentIndexValid(context.currentNodeIndex))
                return TickResult.done;

            var currChild = childList[context.currentNodeIndex];

            var result = currChild.Tick(wData);
            if (result == TickResult.running)
                return result;

            //若子節點運行結束
            context.currentNodeIndex++;
            if (!IsCurrentIndexValid(context.currentNodeIndex))
                //若沒有下一個子節點則返回運行結束
                return TickResult.done;
            return TickResult.running;
        }

        public class SequenceNodeContext : NodeContext
        {
            public int currentNodeIndex;
        }
    }

}