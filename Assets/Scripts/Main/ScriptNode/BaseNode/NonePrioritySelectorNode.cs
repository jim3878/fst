﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Orz.DoTalk
{
    public class ScriptNonePrioritySelectorNode : BaseNode
    {
        public ScriptNonePrioritySelectorNode() : base(-1) { }

        public override bool Evaluate(NodeWorkData wData)
        {
            var context = GetContext<NonePrioritySelectorContext>(wData);
            

            while (IsCurrentIndexValid(context.currNodeIndex))
            {
                var currNode = childList[context.currNodeIndex];
                if (!currNode.Evaluate(wData))
                    context.currNodeIndex++;
                else
                    return true;
            }
            return false;
        }

        public override TickResult Tick(NodeWorkData wData)
        {
            var context = GetContext<NonePrioritySelectorContext>(wData);

            return childList[context.currNodeIndex].Tick(wData);
        }

        public bool IsCurrentIndexValid(int idx)
        {
            return (idx >= 0 && idx < childList.Count);
        }

        public class NonePrioritySelectorContext : NodeContext
        {
            public int currNodeIndex = 0;
        }
    }

    public class NonePrioritySelectorNode : BaseNode
    {
        public NonePrioritySelectorNode() : base(-1) { }

        public override bool Evaluate(NodeWorkData wData)
        {
            var contex = GetContext<NonePrioritySelectorContex>(wData);
            while (IsCurrentIndexValid(contex.currIdx))
            {
                var currNode = childList[contex.currIdx];
                if (!currNode.Evaluate(wData))
                    contex.currIdx++;
                else
                    return true;
            }
            return false;
        }

        public override TickResult Tick(NodeWorkData wData)
        {
            var contex = GetContext<NonePrioritySelectorContex>(wData);
            return childList[contex.currIdx].Tick(wData);
        }

        public bool IsCurrentIndexValid(int idx)
        {
            return (idx >= 0 && idx < childList.Count);
        }

        public class NonePrioritySelectorContex : NodeContext
        {
            public int currIdx = 0;
        }
    }
}