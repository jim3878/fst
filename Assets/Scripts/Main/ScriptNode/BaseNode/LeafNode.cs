﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Orz.DoTalk;
public abstract class LeafNode : BaseNode
{
    public LeafNode() : base(0) { }

    public override bool Evaluate(NodeWorkData workData)
    {
        return true;

    }

}
