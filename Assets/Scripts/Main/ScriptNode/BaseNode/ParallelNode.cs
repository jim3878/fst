﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Orz.DoTalk
{
    public class ParallelNode : BaseNode
    {
        public ParallelNode() : base(-1) { }

        public override bool Evaluate(NodeWorkData wData)
        {
            for (int i = 0; i < this.childList.Count; i++)
            {
                if (!childList[i].Evaluate(wData))
                    return false;
            }
            return true;
        }

        public override TickResult Tick(NodeWorkData wData)
        {
            var contex = GetContext<ParalleContext>(wData);
            if (contex.isFinished == null)
            {
                contex.isFinished = new bool[childList.Count];
                for (int i = 0; i < childList.Count; i++)
                    contex.isFinished[i] = false;
            }

            TickResult result = TickResult.done;
            for (int i = 0; i < this.childList.Count; i++)
            {
                if (contex.isFinished[i])
                    continue;

                var childResult = childList[i].Tick(wData);
                if (childResult == TickResult.done)
                    contex.isFinished[i] = true;

                if (childResult == TickResult.running)
                    result = TickResult.running;
            }

            return result;
        }

        public class ParalleContext : NodeContext
        {
            public bool[] isFinished;
        }
    }
}