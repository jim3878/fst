﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEntity : MonoBehaviour
{
    public enum State
    {
        pickable,
        pick,
        idle,
    }

    public State state;
    public float speed;
    public float moveBuff = 1;
    public Transform pet;
    public SpriteSheet pickable, pick, idle;
    public Vector2 sceneRange, sceneOffset;
    public Pickable pickItem;
    public Action onExit;
    public Action onEnterPick, onEnterPickable, onEnterIdle;
    public bool isTracing;

    public bool isPick;

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    void Init()
    {
        state = State.idle;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isTracing)
            return;
        TickInput();
        TickInRange();
        switch (state)
        {
            case State.idle:
                CanEnterPickable();
                break;
            case State.pickable:
                CanEnterPick();
                break;
            case State.pick:
                if (!CanExitPick())
                    ItemFollow();
                break;
            default:
                break;
        }
    }

    void TickState()
    {
        if (pickItem != null)
            EnterPickable();
    }

    void ItemFollow()
    {
        pickItem.transform.position = transform.position;
    }

    bool CanExitPick()
    {
        if (isPick || pickItem == null)
        {
            if (pickItem != null)
            {
                pickItem.isPicked = false;
                ClearItem();
            }
            EnterIdle();
            isPick = false;
            return true;
        }
        return false;
    }

    void ClearItem()
    {
        pickItem = null;
    }

    bool CanEnterPick()
    {
        if (pickItem != null && isPick)
        {
            EnterPick();
            isPick = false;
            return true;
        }
        return false;
    }

    public void EnterPick()
    {
        ExitState();
        onEnterPick?.Invoke();
        state = State.pick;
        pickItem.isPicked = true;
    }

    private bool CanEnterPickable()
    {
        if (pickItem != null)
        {
            isPick = false;
            EnterPickable();
            return true;
        }
        return false;
    }

    public void EnterIdle()
    {
        ExitState();
        onEnterPickable?.Invoke();
        state = State.idle;
    }

    public void EnterPickable()
    {
        ExitState();
        onEnterPickable?.Invoke();
        state = State.pickable;
    }

    void ExitState()
    {
        onExit?.Invoke();
        onExit = null;
    }

    void TickInput()
    {/*
        if (Input.GetKey(KeyCode.W))
            transform.position += Vector3.up * speed * moveBuff * Time.deltaTime;
        if (Input.GetKey(KeyCode.S))
            transform.position += Vector3.down * speed * moveBuff * Time.deltaTime;
        if (Input.GetKey(KeyCode.A))
            transform.position += Vector3.left * speed * moveBuff * Time.deltaTime;
        if (Input.GetKey(KeyCode.D))
            transform.position += Vector3.right * speed * moveBuff * Time.deltaTime;*/
            var v= Camera.main.ScreenToWorldPoint(Input.mousePosition);
        v.z = transform.position.z;
        transform.position = v;
        //Debug.Log(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        isPick = Input.GetMouseButtonDown(0);//  Input.GetKeyDown(KeyCode.Space);
    }

    void TickInRange()
    {
        sceneOffset.x = pet.position.x;
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, (sceneOffset.x - sceneRange.x / 2), (sceneOffset.x + sceneRange.x / 2)),
            Mathf.Clamp(transform.position.y, (sceneOffset.y - sceneRange.y / 2), (sceneOffset.y + sceneRange.y / 2)),
            transform.position.z);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        var pickable = collision.GetComponent<Pickable>();
        if (pickable == null)
            pickable = collision.GetComponentInParent<Pickable>();
        if (pickable != null && pickItem == null)
        {
            pickItem = pickable;
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        var pickable = collision.GetComponent<Pickable>();
        if (state != State.pick && pickable != null)
        {
            ClearItem();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(sceneOffset, sceneRange);
    }

}
