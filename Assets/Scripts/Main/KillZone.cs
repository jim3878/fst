﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var e = collision.GetComponent<SurviorEntity>();
        if (e != null)
            e.isDie = true;
    }
}
