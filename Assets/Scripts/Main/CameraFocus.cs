﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocus : MonoBehaviour
{
    public Transform targetCam;
    public Vector3 range;
    public float speed = 0.5f;
    [Header("gizmos")]
    public bool showGizmoz = true;
    public float alpha;

    public Transform target;
    public Transform girl;
    public Vector3 centerOffset, moveRange;
    private float _cameraZ;
    private void Start()
    {
        _cameraZ = targetCam.position.z;
    }
    void LateUpdate()
    {
        if (Input.GetKey(KeyCode.W))
            transform.position += Vector3.up * speed * Time.deltaTime;
        if (Input.GetKey(KeyCode.S))
            transform.position += Vector3.down * speed * Time.deltaTime;
        if (Input.GetKey(KeyCode.A))
            transform.position += Vector3.left * speed * Time.deltaTime;
        if (Input.GetKey(KeyCode.D))
            transform.position += Vector3.right * speed * Time.deltaTime;

        centerOffset.x = girl.position.x;
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, (centerOffset.x - moveRange.x / 2), (centerOffset.x + moveRange.x / 2)),
            Mathf.Clamp(transform.position.y, (centerOffset.y - moveRange.y / 2), (centerOffset.y + moveRange.y / 2)),
            transform.position.z);
        /*var moveTarget = targetCam.position;
       if (IsTargetOutleft())
           moveTarget = new Vector3(
               (target.position + Vector3.right * range.x / 2f).x,
               (moveTarget.y));
        if (IsTargetOutRight())
           moveTarget = new Vector3(
               (target.position + Vector3.left * range.x / 2f).x,
               (moveTarget.y));
        if (IsTargetOutTop())
           moveTarget = new Vector3(
               (moveTarget.x),
               (target.position + Vector3.down * range.y / 2f).y);
        if (IsTargetOutDown())
           moveTarget = new Vector3(
               (moveTarget.x),
               (target.position + Vector3.up * range.y / 2f).y);
       TickMoveCamera(moveTarget);*/
    }
    private void OnDrawGizmos()
    {

        if (!showGizmoz)
            return;
        var color = Color.blue;
        color.a = 0.2f;
        Gizmos.color = color;
        Gizmos.DrawWireCube(girl.position + centerOffset, moveRange);
    }
    /*
    private void TickMoveCamera(Vector3 target)
    {
        var result = Vector3.Lerp(targetCam.position, target, moveSpeed);
        result.z = _cameraZ;
        targetCam.position = result;
        //targetCam.position.z = _cameraZ;
    }*/

    private bool IsTargetOutleft()
    {
        return target.position.x < GetRangeLeft();
    }

    private bool IsTargetOutRight()
    {
        return target.position.x > GetRangeRight();
    }

    private bool IsTargetOutTop()
    {
        return target.position.y > GetRangeTop();
    }

    private bool IsTargetOutDown()
    {
        return target.transform.position.y < GetRangeDown();
    }

    private float GetRangeLeft()
    {
        return targetCam.position.x - range.x / 2;
    }

    private float GetRangeRight()
    {
        return targetCam.position.x + range.x / 2;
    }

    private float GetRangeTop()
    {
        return targetCam.position.y + range.y / 2;
    }

    private float GetRangeDown()
    {
        return targetCam.position.y - range.y / 2;
    }



}
