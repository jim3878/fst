﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheetPlayer : MonoBehaviour
{
    public GridAnimator grid;
    // Start is called before the first frame update
    void Start()
    {
        grid = GetComponentInChildren<GridAnimator>();
        var sr = transform.GetChild(0).GetComponent<SpriteRenderer>();
        grid.render = sr;
        grid.startTime = Random.Range(0, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        grid.Tick();
    }
}
