﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarBackground : MonoBehaviour
{
    public Transform targetCam;
    public Transform targetBackground;

    [Range(0, 1)]
    public float distant;
    [SerializeField]
    private Vector3 _camOrgPosition;
    [SerializeField]
    private Vector3 _bgOrgPositoin;
    [SerializeField]
    private Vector3 camDelta;
    private void Start()
    {
        _camOrgPosition = targetCam.transform.position;
        _bgOrgPositoin = targetBackground.position;
    }
    // Update is called once per frame
    void LateUpdate()
    {
        camDelta = targetCam.position - _camOrgPosition;

        targetBackground.position = _bgOrgPositoin + (camDelta * distant);

    }
}
