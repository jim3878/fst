﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridAnimator : MonoBehaviour
{
    public enum State
    {
        running, done
    }
    //public SpritSheet[] spritSheets;
    public int fps = 12;
    [SerializeField] private SpriteSheet _currSheet;
    public float startTime;
    public SpriteRenderer render;
    // Update is called once per frame
    //void Update()
    //{
    //    Tick();
    //}

    public State Tick()
    {
        var currIdx = Mathf.FloorToInt((Time.time - startTime) * fps);
        if (currIdx < 0)
            return State.running;
        if (_currSheet.isLoop)
        {
            render.sprite = _currSheet.sheet[currIdx % _currSheet.length];
            return State.running;
        }
        else
        {
            if (currIdx >= _currSheet.length)
            {
                render.sprite = _currSheet.sheet[_currSheet.length - 1];
                return State.done;
            }
            else
            {
                render.sprite = _currSheet.sheet[currIdx];
                return State.running;
            }
        }
    }

    public void SetCurrSheets(SpriteSheet sheet)
    {
        _currSheet = sheet;
        startTime = Time.time;
    }


}

[System.Serializable]
public class SpriteSheet
{
    public Sprite[] sheet;
    public bool isLoop = true;
    public int length { get { return sheet.Length; } }
}
