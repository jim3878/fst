﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StopTheGirl : MonoBehaviour
{
    public Animation finalScene;
    public SpriteRenderer hand, finalFade;
    public AudioClip steps, wind;
    public AudioSource source, bgm;
    public Camera main, end;
    public PlayerEntity god;
    public SpriteRenderer sr;
    public CameraFocus cf;
    private bool _isStart = false;
    public SurviorEntity pet;
    public bool _stopTrace = false;
    public float topTarget, btmTarget;
    public Transform topBlack, btmBlack;

    private void LateUpdate()
    {
        if (pet != null && !cf.enabled && !_stopTrace)
        {
            Debug.Log("trace");
            var cv = Camera.main.transform.position;
            Camera.main.transform.position = new Vector3(Mathf.Lerp(cv.x, pet.transform.position.x, 0.2f)
                , cv.y, cv.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var se = other.GetComponent<SurviorEntity>();
        if (se != null && !se.isDie)
        {
            if (se.isMyPet)
                pet = se;
            Debug.Log("End");
            StartCoroutine(StopGirl(se));
            if (!_isStart)
            {
                _isStart = true;
                cf.enabled = false;
                StartCoroutine(Ending());
            }

        }
    }

    IEnumerator Ending()
    {
        sr.DOColor(new Color(0, 0, 0, 0.5f), 4f);
        bgm.DOFade(0, 2f);
        yield return new WaitForSeconds(2f);
        god.isTracing = false;
        god.transform.DOPath(new Vector3[] { new Vector3(160f, 5f, -10f), new Vector3(260f, 4f, -10f) }, 4f);
        yield return new WaitForSeconds(2f);
        _stopTrace = true;
        var focusDur = 5f;
        var ot = topBlack.transform.position;
        var ob = btmBlack.transform.position;
        topBlack.DOLocalMoveY(topTarget, focusDur);
        btmBlack.DOLocalMoveY(btmTarget, focusDur);
        Camera.main.transform.DOMove(
            new Vector3(pet.transform.position.x, pet.transform.position.y + 0.2f, Camera.main.transform.position.z)
            , focusDur);
        Camera.main.DOOrthoSize(4, focusDur);
        yield return new WaitForSeconds(focusDur + 1f);
        this.source.clip = steps;
        this.source.panStereo = 0.1f;
        this.source.Play();
        yield return new WaitForSeconds(0.3f);
        Camera.main.DOShakePosition(0.5f, 1, 60, 0);

        yield return new WaitForSeconds(1.5f);
        this.source.clip = steps;
        this.source.panStereo = 0.7f;
        this.source.Play();
        yield return new WaitForSeconds(0.3f);
        Camera.main.DOShakePosition(0.6f, 1, 50, 0);

        yield return new WaitForSeconds(1.8f);
        this.source.clip = steps;
        this.source.panStereo = 0.4f;
        this.source.Play();
        yield return new WaitForSeconds(0.3f);
        Camera.main.DOShakePosition(0.7f, 1, 40, 0);
        yield return new WaitForSeconds(2f);
        var ccDur = 0.5f;
        hand.transform.DOLocalMove(new Vector3(-44.7f, 8.9f, 0.41f), ccDur).SetEase(Ease.InCubic);
        hand.DOColor(new Color(0, 0, 0, 1), ccDur);
        bgm.clip = wind;
        bgm.Play();
        bgm.volume = 0;
        bgm.DOFade(0.5f, 1f).SetDelay(1f);
        yield return new WaitForSeconds(ccDur + 2f);
        finalScene.gameObject.SetActive(true);
        finalFade.DOColor(new Color(0, 0, 0, 0), 0.5f);
        main.enabled = (false);
        end.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        finalScene.Play();
    }

    IEnumerator StopGirl(SurviorEntity se)
    {
        yield return new WaitForSeconds(3f);
        se.isIdle = true;
    }
}
